﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cross_n_circle
{
    public partial class Form1 : Form
    {
        int turn = 0;
        bool player = true;
        int[,] pola = new int[3, 3]; //czesc logiczna
        int temp = 0;
        public Form1()
        {
            InitializeComponent();
        }
        private void action(System.Windows.Forms.Button button)
        {
            if (player) button.Text = "X";
            else button.Text = "O";

            player = !player;
            turn++;
        } //czesc wizualna
        private void win(string direction, int perimeter)
        {
            //najwięcej jebania kodowania z tymi kolorkami
            if (direction == "leftright")
            {
                if (perimeter == 0)
                {
                    nwButton.ForeColor = System.Drawing.Color.Red;
                    nButton.ForeColor = System.Drawing.Color.Red;
                    neButton.ForeColor = System.Drawing.Color.Red;
                }
                else if (perimeter == 1)
                {
                    wButton.ForeColor = System.Drawing.Color.Red;
                    cButton.ForeColor = System.Drawing.Color.Red;
                    eButton.ForeColor = System.Drawing.Color.Red;
                }
                else if (perimeter ==2)
                {
                    swButton.ForeColor = System.Drawing.Color.Red;
                    sButton.ForeColor = System.Drawing.Color.Red;
                    seButton.ForeColor = System.Drawing.Color.Red;
                }
            }
            if (direction == "updown")
            {
                if (perimeter == 0)
                {
                    nwButton.ForeColor = System.Drawing.Color.Red;
                    wButton.ForeColor = System.Drawing.Color.Red;
                    seButton.ForeColor = System.Drawing.Color.Red;
                }
                else if (perimeter == 1)
                {
                    nButton.ForeColor = System.Drawing.Color.Red;
                    cButton.ForeColor = System.Drawing.Color.Red;
                    sButton.ForeColor = System.Drawing.Color.Red;
                }
                else if (perimeter == 2)
                {
                    neButton.ForeColor = System.Drawing.Color.Red;
                    eButton.ForeColor = System.Drawing.Color.Red;
                    seButton.ForeColor = System.Drawing.Color.Red;
                }
            }
            else if (direction == "'\'")
            {
                nwButton.ForeColor = System.Drawing.Color.Red;
                cButton.ForeColor = System.Drawing.Color.Red;
                seButton.ForeColor = System.Drawing.Color.Red;
            }
            else if (direction == "/") 
            {
                    neButton.ForeColor = System.Drawing.Color.Red;
                    cButton.ForeColor = System.Drawing.Color.Red;
                    swButton.ForeColor = System.Drawing.Color.Red;
            }

            if (player) MessageBox.Show("Wygrał gracz O!");
            else MessageBox.Show("Wygrał gracz X!");
            Application.Restart();
        }
        private void check(int x, int y)
        {
            if (turn == 9)
            {
                MessageBox.Show("Remis");
                Application.Restart();
            }

            if (turn > 4)
            {
                //straight
                // -------------------------------------
                for (int i = 0; i < 3; i++)
                {
                    temp += pola[i, y];
                }
                if (temp == 3 || temp == -3) win("leftright", y);
                temp = 0;
                // |||||||||||||||||||||||||||||||||||||||
                for (int i = 0; i < 3; i++)
                {
                    temp += pola[x, i];
                }
                if (temp == 3 || temp == -3) win("updown",x);
                temp = 0;
                //diagonal
                int a=x, b=y;
                // \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
                while (a >= 0 && b >=0)
                {
                    temp += pola[a, b];
                    a--;b--;
                }
                a = x + 1;b = y + 1;
                while (a <= 2 && b <= 2)
                {
                    temp += pola[a, b];
                    a++; b++;
                }

                if (temp == 3 || temp == -3) win("'\'",-1);
                temp = 0;
                // //////////////////////////////////////////////////
                a = x;b = y;
                while (a <= 2 && b >= 0)
                {
                    temp += pola[a, b];
                    a++; b--;
                }
                a = x -1; b = y +1;
                while (a >= 0 && b <= 2)
                {
                    temp += pola[a, b];
                    a--; b++;
                }
                if (temp == 3 || temp == -3) win("/",-1);
                temp = 0;

            }

        }

        private void nwButton_Click(object sender, EventArgs e)
        {
            action(nwButton);
            if (player) pola[0, 0] = -1; else pola[0, 0] = 1;
            check(0,0);
        }

        private void nButton_Click(object sender, EventArgs e)
        {
            action(nButton);
            if (player) pola[1, 0] = -1; else pola[1, 0] = 1;
            check(1,0);
        }

        private void neButton_Click(object sender, EventArgs e)
        {
            action(neButton);
            if (player) pola[2, 0] = -1; else pola[2, 0] = 1;
            check(2,0);
        }

        private void wButton_Click(object sender, EventArgs e)
        {
            action(wButton);
            if (player) pola[0, 1] = -1; else pola[0, 1] = 1;
            check(0,1);
        }

        private void cButton_Click(object sender, EventArgs e)
        {
            action(cButton);
            if (player) pola[1, 1] = -1; else pola[1, 1] = 1;
            check(1,1);
        }

        private void eButton_Click(object sender, EventArgs e)
        {
            action(eButton);
            if (player) pola[2, 1] = -1; else pola[2, 1] = 1;
            check(2,1);
        }

        private void swButton_Click(object sender, EventArgs e)
        {
            action(swButton);
            if (player) pola[0, 2] = -1; else pola[0, 2] = 1;
            check(0,2);
        }

        private void sButton_Click(object sender, EventArgs e)
        {
            action(sButton);
            if (player) pola[1, 2] = -1; else pola[1, 2] = 1;
            check(1,2);
        }

        private void seButton_Click(object sender, EventArgs e)
        {
            action(seButton);
            if (player) pola[2, 2] = -1; else pola[2, 2] = 1;
            check(2,2);
        }
    }
}
