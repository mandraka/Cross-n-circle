﻿namespace Cross_n_circle
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nwButton = new System.Windows.Forms.Button();
            this.nButton = new System.Windows.Forms.Button();
            this.neButton = new System.Windows.Forms.Button();
            this.eButton = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.wButton = new System.Windows.Forms.Button();
            this.swButton = new System.Windows.Forms.Button();
            this.sButton = new System.Windows.Forms.Button();
            this.seButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nwButton
            // 
            this.nwButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nwButton.Location = new System.Drawing.Point(227, 76);
            this.nwButton.Name = "nwButton";
            this.nwButton.Size = new System.Drawing.Size(70, 70);
            this.nwButton.TabIndex = 0;
            this.nwButton.UseVisualStyleBackColor = true;
            this.nwButton.Click += new System.EventHandler(this.nwButton_Click);
            // 
            // nButton
            // 
            this.nButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nButton.Location = new System.Drawing.Point(303, 76);
            this.nButton.Name = "nButton";
            this.nButton.Size = new System.Drawing.Size(70, 70);
            this.nButton.TabIndex = 1;
            this.nButton.UseVisualStyleBackColor = true;
            this.nButton.Click += new System.EventHandler(this.nButton_Click);
            // 
            // neButton
            // 
            this.neButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.neButton.Location = new System.Drawing.Point(379, 76);
            this.neButton.Name = "neButton";
            this.neButton.Size = new System.Drawing.Size(70, 70);
            this.neButton.TabIndex = 2;
            this.neButton.UseVisualStyleBackColor = true;
            this.neButton.Click += new System.EventHandler(this.neButton_Click);
            // 
            // eButton
            // 
            this.eButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.eButton.Location = new System.Drawing.Point(379, 152);
            this.eButton.Name = "eButton";
            this.eButton.Size = new System.Drawing.Size(70, 70);
            this.eButton.TabIndex = 3;
            this.eButton.UseVisualStyleBackColor = true;
            this.eButton.Click += new System.EventHandler(this.eButton_Click);
            // 
            // cButton
            // 
            this.cButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cButton.Location = new System.Drawing.Point(303, 152);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(70, 70);
            this.cButton.TabIndex = 4;
            this.cButton.UseVisualStyleBackColor = true;
            this.cButton.Click += new System.EventHandler(this.cButton_Click);
            // 
            // wButton
            // 
            this.wButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wButton.Location = new System.Drawing.Point(227, 152);
            this.wButton.Name = "wButton";
            this.wButton.Size = new System.Drawing.Size(70, 70);
            this.wButton.TabIndex = 5;
            this.wButton.UseVisualStyleBackColor = true;
            this.wButton.Click += new System.EventHandler(this.wButton_Click);
            // 
            // swButton
            // 
            this.swButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.swButton.Location = new System.Drawing.Point(227, 228);
            this.swButton.Name = "swButton";
            this.swButton.Size = new System.Drawing.Size(70, 70);
            this.swButton.TabIndex = 6;
            this.swButton.UseVisualStyleBackColor = true;
            this.swButton.Click += new System.EventHandler(this.swButton_Click);
            // 
            // sButton
            // 
            this.sButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.sButton.Location = new System.Drawing.Point(303, 228);
            this.sButton.Name = "sButton";
            this.sButton.Size = new System.Drawing.Size(70, 70);
            this.sButton.TabIndex = 7;
            this.sButton.UseVisualStyleBackColor = true;
            this.sButton.Click += new System.EventHandler(this.sButton_Click);
            // 
            // seButton
            // 
            this.seButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.seButton.Location = new System.Drawing.Point(379, 228);
            this.seButton.Name = "seButton";
            this.seButton.Size = new System.Drawing.Size(70, 70);
            this.seButton.TabIndex = 8;
            this.seButton.UseVisualStyleBackColor = true;
            this.seButton.Click += new System.EventHandler(this.seButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(483, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Twój ruch!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.seButton);
            this.Controls.Add(this.sButton);
            this.Controls.Add(this.swButton);
            this.Controls.Add(this.wButton);
            this.Controls.Add(this.cButton);
            this.Controls.Add(this.eButton);
            this.Controls.Add(this.neButton);
            this.Controls.Add(this.nButton);
            this.Controls.Add(this.nwButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button nwButton;
        private System.Windows.Forms.Button nButton;
        private System.Windows.Forms.Button neButton;
        private System.Windows.Forms.Button eButton;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button wButton;
        private System.Windows.Forms.Button swButton;
        private System.Windows.Forms.Button sButton;
        private System.Windows.Forms.Button seButton;
        private System.Windows.Forms.Label label1;
    }
}

